import os
from pathlib import Path
from striprtf.striprtf import rtf_to_text
directory = Path.home()/"Library/Containers/com.apple.Stickies/Data/Library/Stickies"
os.mkdir("./Notes")
for i,file in enumerate(os.listdir(directory)):
	if not file.startswith("."):
		f = open(os.path.join(directory,file,"TXT.rtf"),"r").read()
		plaintext = rtf_to_text(f)
		n = open("./Notes/NOTE_"+str(i+1)+".txt", "w")
		n.write(plaintext)