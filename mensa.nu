#!/usr/bin/env nu

def main [mensa:string] {
	let date = ( date now | format date "%Y-%m-%d")

	let data = (http get $"https://www.studentenwerk-goettingen.de/fileadmin/templates/php/mensaspeiseplan/cached/de/($date)/($mensa).html" | tidy -asxhtml -quiet --show-warnings no | from xml).content.1.content.0.content.0.content.0.content



	for $d in ($data | skip 1) {
	 	try {
	 		print (get_content $d).content
	 	} catch {
	 	}
	}

}
def get_content [r:record] {
	$r.content.1.content.0.content
}